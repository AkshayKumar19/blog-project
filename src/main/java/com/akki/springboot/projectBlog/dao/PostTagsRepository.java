package com.akki.springboot.projectBlog.dao;

import com.akki.springboot.projectBlog.model.PostTags;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostTagsRepository extends JpaRepository<PostTags, Integer> {
}
