package com.akki.springboot.projectBlog.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "post_tags")
public class PostTags {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "post_id")
    private int postId;
    @Column(name = "tag_id")
    private int tagId;
    @Column(name = "created_at")
    private LocalDateTime creationTime;
    @Column(name = "updated_at")
    private LocalDateTime updateTime;

    public PostTags() {
    }

    public PostTags(int postId, int tagId) {
        this.postId = postId;
        this.tagId = tagId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "PostTags{" +
                "id=" + id +
                ", postId=" + postId +
                ", tagId=" + tagId +
                ", creationTime=" + creationTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
