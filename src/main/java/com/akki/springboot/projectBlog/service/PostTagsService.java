package com.akki.springboot.projectBlog.service;

import com.akki.springboot.projectBlog.model.PostTags;

import java.util.List;

public interface PostTagsService {

    public List<PostTags> findAll();
    public PostTags findPostTagsById(int id);
    public void save(PostTags postTags);
    public void deleteById(int postTagId);
}
