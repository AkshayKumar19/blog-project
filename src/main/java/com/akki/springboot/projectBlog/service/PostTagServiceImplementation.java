package com.akki.springboot.projectBlog.service;

import com.akki.springboot.projectBlog.dao.PostTagsRepository;
import com.akki.springboot.projectBlog.dao.PostsRepository;
import com.akki.springboot.projectBlog.model.PostTags;
import com.akki.springboot.projectBlog.model.Posts;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class PostTagServiceImplementation implements PostTagsService{

    private PostTagsRepository postTagsRepository;

    public PostTagServiceImplementation(PostTagsRepository postTagsRepository) {
        this.postTagsRepository = postTagsRepository;
    }

    @Override
    public List<PostTags> findAll() {
        return postTagsRepository.findAll();
    }

    @Override
    public PostTags findPostTagsById(int id) {
        Optional<PostTags> result = postTagsRepository.findById(id);

        PostTags postTags = null;
        if(result.isPresent()){
            postTags = result.get();
        }else {
            throw new RuntimeException("Posts Tag id not available " + id);
        }
        return postTags;
    }

    @Override
    public void save(PostTags postTags) {
        postTagsRepository.save(postTags);
    }

    @Override
    public void deleteById(int postTagId) {
        postTagsRepository.deleteById(postTagId);
    }
}
