package com.akki.springboot.projectBlog.service;

import com.akki.springboot.projectBlog.model.Posts;

import java.util.List;

public interface PostService {

    public List<Posts> findAll();
    public Posts findPostsById(int id);
    public void save(Posts posts);
    public void deleteById(int postId);
    public List<Posts> search(String searching);
    List<Posts> sort(String dateAsc);
}