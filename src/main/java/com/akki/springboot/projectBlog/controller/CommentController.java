package com.akki.springboot.projectBlog.controller;

import com.akki.springboot.projectBlog.model.Comments;
import com.akki.springboot.projectBlog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/comments")
public class CommentController {
    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/showComments")
    public String viewComments(Model model){
        List<Comments> commentList = commentService.findAll();

        model.addAttribute("commentList", commentList);
        return "comments/commentsPage";
    }

    @GetMapping("/showCommentForm")
    public String showCommentForm(Model model){
        Comments comments = new Comments();

        model.addAttribute("commentForm", comments);
        return "comments/commentForm";
    }

    @PostMapping("/showCommentFormForUpdate")
    public String showCommentFormForUpdate(@RequestParam("commentId") int id, Model model){

        Comments comments = commentService.findCommentsById(id);

        model.addAttribute("commentForm", comments);
        return "comments/commentForm";
    }

    @PostMapping("/saveComment")
    public String saveComment(@ModelAttribute("commentForm") Comments comments){
        //save comments
        commentService.save(comments);
        System.out.println("Comments data: " + comments);
        return "redirect:/comments/showComments";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam("commentId") int id, Model model){
        commentService.deleteById(id);
        return "redirect:/comments/showComments";
    }
}
